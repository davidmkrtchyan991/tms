<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

    protected $table = 'tasks';

    public function created_by_user() {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function assigned_to_user() {
        return $this->belongsTo('App\User', 'assigned_to', 'id');
    }
}
