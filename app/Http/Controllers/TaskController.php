<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class TaskController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::with('created_by_user', 'assigned_to_user')->get();

        return view('home')->with('tasks', $tasks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'status' => 'required',
            'assigned_to' => 'required',
        ]);

        if ($validatedData) {
            $task = new Task;

            $task->assigned_to = $request->assigned_to;
            $task->title = $request->title;
            $task->description = $request->description;
            $task->status = $request->status;
            $task->created_by = Auth::id();
            $task->save();

            return redirect('home')->with('success', 'New post was created');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::find($id);
        return view('tasks.show')->with('task', $task);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::with('assigned_to_user')->find($id);
        return view('tasks.edit')->with('task', $task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Task $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'status' => 'required',
            'assigned_to' => 'required',
        ]);

        if($validatedData) {
            $task = Task::find($id);

            $task->assigned_to = $request->assigned_to;
            $task->title = $request->title;
            $task->description = $request->description;
            $task->status = $request->status;
            $task->save();
            return redirect('home')->with('success', 'Information has been  edited');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        $task = Task::find($id);
        $task->delete();

        return redirect('home')->with('success', 'Information has been  deleted');
    }
}
