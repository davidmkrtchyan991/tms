<?php

namespace App\Http\Controllers;

use App\Task;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tasks = Task::with('created_by_user', 'assigned_to_user')->get();

        return view('home')->with('tasks', $tasks);
    }

    public function usersList(Request $request)
    {
        if ($request->ajax()) {

            $users = User::where('name', 'LIKE', '%' . $request->search . "%")->orWhere('description', 'LIKE', '%' . $request->search . "%")->get()->except(Auth::id());
        }
        return $users;
    }
}
