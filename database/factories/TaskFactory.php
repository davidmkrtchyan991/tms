<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Task::class, function (Faker $faker) {

    return [
        'created_by' => $faker->numberBetween($min = 1, $max = 50),
        'assigned_to' => $faker->numberBetween($min = 1, $max = 50),
        'status' => $faker->randomElement($array = array ('pending','completed')),
        'title' => "Title ". $faker->word,
        'description' => $faker->sentence($nbWords = 6, $variableNbWords = true),
    ];
});
