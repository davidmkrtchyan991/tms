$('#assigned_to').on('keyup', function () {
    let value = $(this).val();
    let output = '';
    $.ajax({
        type: 'get',
        url: "/users-list",
        data: {'search': value},
        success: function (data) {

            if (data) {
                for (let user in data) {
                    if (data.hasOwnProperty(user)) {
                        output += '<tr class="users">' +
                            '<td class="userId" data-id="' + data[user].id + '">' + data[user].id + '</td>' +
                            '<td  class="userName" data-name="' + data[user].name + '">' + data[user].name + '</td>' +
                            '<td>' + data[user].email + '</td>' +
                            '</tr>';
                    }
                }
            }
            $('tbody').html(output).show();
            initUserSelection();
        }
    });
});


function initUserSelection() {
    $('.table tbody tr').click(function() {
        let userId = $(this).find('.userId').data('id');
        let userName = $(this).find('.userName').data('name');
        $('.user_id').val(userId);
        $('#assigned_to').val(userName);
        $(this).closest('tbody').hide();
        console.log(userId + ' ' + userName);
    });
}



$('#task_search').on('keyup', function () {
    var value = $(this).val().toLowerCase();
    $("#tasks tbody tr").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
});
