@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">New task
                    <a href="tasks/create" type="button" class="btn btn-primary" style="float: right;">New task</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form method="post" action="{{action('TaskController@update', $task->id)}}">
                        @csrf
                        <input name="_method" type="hidden" value="PATCH">

                        <div class="form-group {{Auth::user()->id !== $task->created_by_user->id ? 'd-none' : '' }}">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title" id="title" placeholder="Title"
                                   value="{{$task->title}}">
                        </div>

                        <div class="form-group {{Auth::user()->id !== $task->created_by_user->id ? 'd-none' : '' }}">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="description" name="description"
                                      placeholder="Description">{{$task->description}}</textarea>
                        </div>

                        <div class="form-group {{Auth::user()->id === $task->assigned_to_user->id ? '' : 'd-none' }}">
                            <label for="status">Status</label>

                            <select class="form-control" name="status" id="status">
                                <option {{($task->status === 'pending') ? 'selected' : '' }} value="pending">Pending
                                </option>
                                <option {{($task->status === 'completed') ? 'selected' : '' }} value="completed">
                                    Completed
                                </option>
                            </select>
                        </div>

                        <div class="form-group {{Auth::user()->id !== $task->created_by_user->id ? 'd-none' : '' }}">
                            <label for="assigned_to">Assign to</label>
                            <input type="text" class="form-control " id="assigned_to"
                                   value="{{$task->assigned_to_user->name}}">
                            <input name="assigned_to" type="hidden" class="user_id"
                                   value="{{$task->assigned_to_user->id}}">
                        </div>
                        <table class="table table-bordered table-hover">
                            <tbody>
                            </tbody>
                        </table>

                        <button type="submit" class="btn btn-primary" style="float: right;">Submit</button>
                    </form>

                    <form action="{{action('TaskController@destroy', $task->id)}}" method="post"
                          style="display: inline-block; float: left;">
                        @csrf
                        <input name="_method" type="hidden" value="DELETE">
                        <button class="btn btn-danger"
                                type="submit" {{Auth::user()->id !== $task->created_by_user->id ? 'disabled' : '' }}>
                            Delete
                        </button>
                    </form>

                </div>
            </div>
        </div>
    </div>


@endsection
