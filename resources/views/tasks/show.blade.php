@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Task {{$task->id}}
                    <a href="tasks/create" type="button" class="btn btn-primary" style="float: right;">New task</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#ID</th>
                            <th scope="col">Title</th>
                            <th scope="col">Description</th>
                            <th scope="col">Created by</th>
                            <th scope="col">Assigned to</th>
                            <th scope="col">Status</th>
                            <th scope="col">Created at</th>
                            <th scope="col" style="width: 170px;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <th scope="row">{{$task->id}}</th>
                            <td>{{$task->title}}</td>
                            <td>{{$task->description}}</td>
                            <td>{{$task->created_by_user->name}}</td>
                            <td>{{$task->assigned_to_user->name}}</td>
                            <td>{{$task->status}}</td>
                            <td>{{$task->created_at}}</td>
                            <td>
                                @if(Auth::user()->id === $task->created_by_user->id)
                                    <a class="btn btn-success" href="{{$task->id}}/edit" role="button">Edit</a>
                                    <a class="btn btn-danger" href="{{$task->id}}/show" role="button">Delete</a>
                                @endif
                                @if(Auth::user()->id === $task->assigned_to_user->id)
                                    <a class="btn btn-warning" href="{{$task->id}}/show" role="button">Edit</a>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>

@endsection
