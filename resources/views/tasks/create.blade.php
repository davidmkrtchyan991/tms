@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">New task
                    <a href="tasks/create" type="button" class="btn btn-primary" style="float: right;">New task</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form method="post" action="{{url('tasks')}}">
                        @csrf
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title" id="title" placeholder="Title">
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="description" name="description"
                                      placeholder="Description"></textarea>
                        </div>


                        <div class="form-group">
                            <label for="status">Status</label>
                            <select class="form-control" id="status" name="status">
                                <option selected="selected" value="pending">Pending</option>
                                <option value="completed">Completed</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="assigned_to">Assign to</label>
                            <input type="text" class="form-control" id="assigned_to">
                            <input name="assigned_to" type="hidden" class="user_id" value="">
                        </div>
                        <table class="table table-bordered table-hover">
                            <tbody>
                            </tbody>
                        </table>

                        <button type="submit" class="btn btn-primary" style="float: right;">Submit</button>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
